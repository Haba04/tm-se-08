package ru.habibrahmanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.api.IProjectRepository;
import ru.habibrahmanov.tm.api.IProjectService;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.entity.Project;

import java.util.List;
import java.util.UUID;

public final class ProjectService implements IProjectService {
    @NotNull private final IProjectRepository iProjectRepository;

    public ProjectService(@NotNull IProjectRepository iProjectRepository) {
        this.iProjectRepository = iProjectRepository;
    }

    @Override
    public void persist(@Nullable final String projectName, @Nullable final String userId) throws IncorrectValueException {
        if (projectName == null || projectName.isEmpty()){
            throw new IncorrectValueException();
        }
        iProjectRepository.persist(new Project(projectName, UUID.randomUUID().toString(), userId));
    }

    @Override
    public List<Project> findAll(@NotNull final String userId) throws IncorrectValueException {
        if (iProjectRepository.findAll(userId).isEmpty()) {
            throw new IncorrectValueException("PROJECT LIST EMPTY");
        }
        return iProjectRepository.findAll(userId);
    }

    @Override
    public void removeAll() throws IncorrectValueException {
        if (iProjectRepository.getProjectMap().isEmpty()) {
            throw new IncorrectValueException("PROJECT LIST EMPTY");
        }
        iProjectRepository.removeAll();
    }

    @Override
    public void remove(@Nullable final String projectId) throws IncorrectValueException {
        if (projectId == null || projectId.isEmpty()) {
            throw new IncorrectValueException();
        }
        iProjectRepository.remove(projectId);
    }

    @Override
    public void update(@Nullable final String userId, @Nullable final String projectId, @Nullable final String name) throws IncorrectValueException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (projectId == null || projectId.isEmpty()) throw new IncorrectValueException();
        if (name == null || name.isEmpty()) throw new IncorrectValueException();
        iProjectRepository.update(userId, projectId, name);
    }

    @Override
    public void merge(@Nullable final String projectId, @Nullable final String name, @Nullable final String userId) throws IncorrectValueException {
        if (projectId == null || projectId.isEmpty() || name == null || name.isEmpty()) {
            throw new IncorrectValueException();
        }
        iProjectRepository.merge(new Project(name, projectId, userId));
    }

    public IProjectRepository getiProjectRepository() {
        return iProjectRepository;
    }
}
