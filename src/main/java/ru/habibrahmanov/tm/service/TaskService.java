package ru.habibrahmanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.api.ITaskRepository;
import ru.habibrahmanov.tm.api.ITaskService;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.entity.Task;

import java.util.List;
import java.util.UUID;

public final class TaskService implements ITaskService {
    @NotNull
    private final ITaskRepository iTaskRepository;

    public TaskService(@NotNull ITaskRepository iTaskRepository) {
        this.iTaskRepository = iTaskRepository;
    }

    @Override
    public void persist(@Nullable final String projectId, @Nullable final String userId, @Nullable final String name) throws IncorrectValueException {
        if (projectId == null || projectId.isEmpty()) {
            throw new IncorrectValueException();
        }
        iTaskRepository.persist(new Task(name, UUID.randomUUID().toString(), projectId, userId));
    }

    @Override
    public Task findOne(@Nullable final String taskId, @NotNull final String userId) throws IncorrectValueException {
        if (taskId == null || taskId.isEmpty()) {
            throw new IncorrectValueException();
        }
        return iTaskRepository.findOne(taskId, userId);
    }

    @Override
    public List<Task> findAll(@NotNull final String userId) throws IncorrectValueException {
        if (iTaskRepository.findAll(userId).isEmpty()) {
            throw new IncorrectValueException("LIST IS EMPTY");
        }
        return iTaskRepository.findAll(userId);
    }

    @Override
    public void remove(@Nullable final String taskId) throws IncorrectValueException {
        if (taskId == null || taskId.isEmpty()) {
            throw new IncorrectValueException();
        }
        iTaskRepository.remove(taskId);
    }

    @Override
    public void removeAll(@NotNull final String projectId) {
        iTaskRepository.removeAll(projectId);
    }

    @Override
    public void update(@Nullable final String userId, @Nullable final String taskId, @Nullable final String name) throws IncorrectValueException {
        if (userId == null || userId.isEmpty() || taskId == null || taskId.isEmpty() || name == null || name.isEmpty()) {
            throw new IncorrectValueException();
        }
        iTaskRepository.update(userId, taskId, name);
    }

    @Override
    public void merge(@Nullable final String projectId, @Nullable final String taskId, @Nullable final String name, @Nullable final String userId) throws IncorrectValueException {
        if (taskId == null || taskId.isEmpty() || name == null || name.isEmpty()) {
            throw new IncorrectValueException();
        }
        iTaskRepository.merge(new Task(name, taskId, projectId, userId));
    }
}
