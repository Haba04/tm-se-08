package ru.habibrahmanov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.api.*;
import ru.habibrahmanov.tm.repository.ProjectRepository;
import ru.habibrahmanov.tm.repository.TaskRepository;
import ru.habibrahmanov.tm.repository.UserRepository;
import ru.habibrahmanov.tm.service.ProjectService;
import ru.habibrahmanov.tm.service.TaskService;
import ru.habibrahmanov.tm.command.AbstractCommand;
import ru.habibrahmanov.tm.service.UserService;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public final class Bootstrap implements ServiceLocator {
    @NotNull private final IProjectRepository projectRepository = new ProjectRepository();
    @NotNull private final ITaskRepository taskRepository = new TaskRepository();
    @NotNull private final IUserRepository userRepository = new UserRepository();
    @NotNull private final IProjectService projectService = new ProjectService(projectRepository);
    @NotNull private final ITaskService taskService = new TaskService(taskRepository);
    @NotNull private final IUserService userService = new UserService(userRepository);
    @NotNull private final Scanner scanner = new Scanner(System.in);
    @NotNull private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public void init(final Class... commandsClass) {
        try {
            if (commandsClass == null) return;
            for (Class clazz : commandsClass) {
                registry(clazz);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void registry(@NotNull final Class clazz) throws IllegalAccessException, InstantiationException {
        AbstractCommand abstractCommand = (AbstractCommand) clazz.newInstance();
        abstractCommand.setBootstrap(this);
        @Nullable final String nameCommand = abstractCommand.getName();
        @Nullable final String descriptionCommand = abstractCommand.getDescription();
        if (nameCommand == null || nameCommand.isEmpty()) return;
        if (descriptionCommand == null || descriptionCommand.isEmpty()) return;
        commands.put(nameCommand, abstractCommand);
    }

    public void start() throws Exception {
        System.out.println("***WELCOME TO TASK MANAGER***");
        System.out.println("Enter \"help\" to show all commands.");
        String command = "";
        while (!command.equals("exit")) {
            command = scanner.nextLine();
            execute(command);
        }
    }

    private void execute(@Nullable final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        if (abstractCommand.secure() || (!abstractCommand.secure() && userService.isAuth())) {
            abstractCommand.execute();
        }
    }

    @Override
    public Map<String, AbstractCommand> getCommands() {
        return commands;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public Scanner getScanner() {
        return scanner;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }
}
