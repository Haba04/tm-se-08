package ru.habibrahmanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.api.IUserRepository;
import ru.habibrahmanov.tm.entity.User;

import java.util.*;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {
    @Override
    public void persist(@NotNull final User user) {
        entities.put(user.getId(), user);
    }

    @Override
    public User findOne(@NotNull final String userId) {
        return entities.get(userId);
    }

    @Override
    public User findByLogin(@NotNull final String login) {
        User user = null;
        List<User> userList = new ArrayList<>(entities.values());
        for (int i = 0; i < userList.size(); i++) {
            if (userList.get(i).getLogin().equals(login))
                user =  userList.get(i);
        }
        return user;
    }

    @Override
    public List<User> findAll() {
        List<User> userList = new ArrayList<>(entities.values());
        return userList;
    }

    @Override
    public void removeOne(@NotNull final String login) {
        entities.remove(login);
    }

    @Override
    public void removeAll() {
        entities.clear();
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final String login) {
        entities.get(userId).setLogin(login);
    }

    @Override
    public void merge(User user) {

    }
}
