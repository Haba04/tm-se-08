package ru.habibrahmanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.api.IProjectRepository;
import ru.habibrahmanov.tm.entity.Project;

import java.util.*;


public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {
    public Map<String, Project> getProjectMap() {
        return entities;
    }

    @Override
    public void persist(@NotNull final Project project) {
        entities.put(project.getId(), project);
    }

    @Override
    public List<Project> findAll(@NotNull final String userId) {
        List<Project> projectList = new ArrayList<>();
        for (Project project : entities.values()) {
            if (project.getUserId().equals(userId)) {
                projectList.add(project);
            }
        }
        return projectList;
    }

    @Override
    public Project findOne(@NotNull String projectId) {
        return entities.get(projectId);
    }

    @Override
    public void removeAll() {
        entities.clear();
    }

    @Override
    public void remove(@NotNull final String projectId) {
        entities.remove(projectId);
    }

    @Override
    public void merge(@NotNull final Project project) {
        if (entities.containsKey(project.getId())) {
            update(project.getUserId(), project.getId(), project.getName());
        }
        persist(project);
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final String projectId, @NotNull final String name) {
        if (entities.get(projectId).getUserId().equals(userId)) {
            entities.get(projectId).setName(name);
        }
    }
}

