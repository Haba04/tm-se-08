package ru.habibrahmanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.api.ITaskRepository;
import ru.habibrahmanov.tm.entity.Task;

import java.util.*;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public void persist(@NotNull final Task task) {
        entities.put(task.getId(), task);
    }

    @Override
    public Task findOne(@NotNull final String taskId, @NotNull final String userId) {
        for (Task task : entities.values()) {
            if (task.getUserId().equals(userId)) {
                entities.get(taskId);
            }
        }
        return null;
    }

    @Override
    public List<Task> findAll(@NotNull final String userId) {
        final List<Task> taskList = new ArrayList<>();
        for (Task task : entities.values()){
            if (task.getUserId().equals(userId))
            taskList.add(task);
        }
        return taskList;
    }

    @Override
    public void remove(@NotNull final String taskId) {
        entities.remove(taskId);
    }

    @Override
    public boolean removeAll(@NotNull final String projectId) {
        Iterator<Map.Entry<String, Task>> iterator = entities.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Task> entryTask = iterator.next();
            if (entryTask.getValue().getProjectId().equals(projectId)) {
                iterator.remove();
            }
        }
        return false;
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final String taskId, @NotNull final String name) {
        if (entities.get(taskId).getUserId().equals(userId)) {
            entities.get(taskId).setName(name);
        }
    }

    @Override
    public void merge(@NotNull final Task task) {
        if (entities.containsKey(task.getId())) {
            update(task.getUserId(), task.getId(), task.getName());
        } else {
            persist(task);
        }
    }
}

