package ru.habibrahmanov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.entity.User;

import java.util.List;

public interface IUserRepository {
    void persist(@NotNull User user);

    User findOne(@NotNull String userId);

    User findByLogin(@NotNull String login);

    List<User> findAll();

    void removeOne(@NotNull String login);

    void removeAll();

    void update(@NotNull String userId, @NotNull String login);
}
