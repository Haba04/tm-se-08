package ru.habibrahmanov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {
    void persist(@NotNull Task task);

    Task findOne(@NotNull String taskId, @NotNull String userId);

    List<Task> findAll(@NotNull String userId);

    void remove(@NotNull String taskId);

    boolean removeAll(@NotNull String projectId);

    void update(@NotNull String userId, @NotNull String taskId, @NotNull String name);

    void merge(@NotNull Task task);
}
