package ru.habibrahmanov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.entity.Task;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;

import java.util.List;

public interface ITaskService {
    void persist(@Nullable String projectId, @Nullable String userId, @Nullable String name) throws IncorrectValueException;

    Task findOne(@Nullable String taskId, @NotNull String userId) throws IncorrectValueException;

    List<Task> findAll(@NotNull String userId) throws IncorrectValueException;

    void remove(@Nullable String taskId) throws IncorrectValueException;

    void removeAll(@NotNull String projectId);

    void update(@Nullable String userId, @Nullable String taskId, @Nullable String name) throws IncorrectValueException;

    void merge(@Nullable String projectId, @Nullable String taskId, @Nullable String name, @Nullable String userId) throws IncorrectValueException;
}
