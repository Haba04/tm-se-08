package ru.habibrahmanov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.entity.Project;

import java.util.List;
import java.util.Map;

public interface IProjectRepository {
    void persist(@NotNull Project project);

    List<Project> findAll(@NotNull String userId);

    Project findOne(@NotNull String projectId);

    void removeAll();

    void remove(@NotNull String projectId);

    void merge(@NotNull Project project);

    void update(@NotNull String userId, @NotNull String projectId, @NotNull String name);

    public Map<String, Project> getProjectMap();
}
