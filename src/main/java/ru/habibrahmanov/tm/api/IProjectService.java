package ru.habibrahmanov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;

import java.util.List;

public interface IProjectService {
    void persist(@Nullable String projectName, @Nullable String userId) throws IncorrectValueException;

    List<Project> findAll(@NotNull String userId) throws IncorrectValueException;

    void removeAll() throws IncorrectValueException;

    void remove(@Nullable String projectId) throws IncorrectValueException;

    void update(@Nullable String userId, @Nullable String projectId, @Nullable String name) throws IncorrectValueException;

    void merge(@Nullable String projectId, @Nullable String name, @Nullable String userId) throws IncorrectValueException;
}
