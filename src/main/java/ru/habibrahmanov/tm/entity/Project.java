package ru.habibrahmanov.tm.entity;

import lombok.Getter;
import lombok.Setter;
import java.util.Date;

@Getter
@Setter
public final class Project {
    private String name;
    private String description;
    private String id;
    private String userId;
    private Date dateBegin;
    private Date dateEnd;

    public Project() {
    }

    public Project(String name, String id, String userId) {
        this.name = name;
        this.id = id;
        this.userId = userId;
    }
}
