package ru.habibrahmanov.tm.entity;

import lombok.Getter;
import lombok.Setter;
import java.util.Date;

@Getter
@Setter
public final class Task {
    private String name;
    private String description;
    private String id;
    private String projectId;
    private String userId;
    private Date dateBegin;
    private Date dateEnd;

    public Task() {
    }

    public Task(String name, String id, String projectId, String userId) {
        this.name = name;
        this.id = id;
        this.projectId = projectId;
        this.userId = userId;
    }
}
