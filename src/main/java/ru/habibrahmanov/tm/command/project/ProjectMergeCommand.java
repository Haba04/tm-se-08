package ru.habibrahmanov.tm.command.project;

import ru.habibrahmanov.tm.command.AbstractCommand;

public final class ProjectMergeCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-merge";
    }

    @Override
    public String getDescription() {
        return "update if project is already exist, else create new project";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT MERGE]");
        System.out.println("ENTER PROJECT ID");
        final String projectId = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER PROJECT NAME");
        final String name = serviceLocator.getScanner().nextLine();
        final String userId = serviceLocator.getUserService().getCurrentUser().getId();
        serviceLocator.getProjectService().merge(projectId, name, userId);
        System.out.println("PROJECT MERGE SUCCESSFULLY");
    }
}
