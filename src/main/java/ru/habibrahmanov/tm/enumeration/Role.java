package ru.habibrahmanov.tm.enumeration;

public enum Role {
    ADMIN("admin"),
    USER("user");

    private String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

    public String displayName() {
        return displayName;
    }
}
